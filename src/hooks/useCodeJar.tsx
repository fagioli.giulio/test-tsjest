import {useRef, useEffect} from "preact/hooks";

export const useCodeJar = () => {
  const refToEditor = useRef<HTMLDivElement>(null);
  return refToEditor;
}