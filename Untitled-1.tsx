import '@testing-library/jest-dom/extend-expect'
import { h,Ref} from 'preact';
import { render } from '@testing-library/preact';
import {useCodeJar} from "../src";
import {useEffect} from "preact/hooks";

describe('useCodeJar', () => {
  it('useCodeJar target correct element | useRef', () => {
    let editorRef;
    const CodeEditor = () => {
      editorRef = useCodeJar({});
        return (<div id="codejar" ref={editorRef} />)
    }
     
    const {container} = render(CodeEditor);
    const ref = {
      current: container.querySelector("#codejar")
    }
    expect(ref.current).toBe(editorRef);
    expect(ref?.current?.getAttribute("contentEditable")).toBe(true);
  });
});




import {useRef, useEffect} from "preact/hooks";
import {CodeJar}  from "@medv/codejar";
import {Ref} from "preact";

interface Props {
  code?: string;
}

export const useCodeJar = (props: Props) => {
  const refToEditor = useRef<HTMLDivElement>(null);
  let editor = null;
  useEffect(()=>{
    editor = CodeJar(refToEditor.current, (e: HTMLElement) =>{}, {});
  },[refToEditor.current])
  return refToEditor;
}