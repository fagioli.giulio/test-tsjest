import {useRef, useEffect} from "preact/hooks";
import {CodeJar}  from "@medv/codejar";
import {Ref} from "preact";

interface Props {
  code?: string;
}

export const useCodeJar = (props: Props) => {
  const refToEditor = useRef<HTMLDivElement>(null);
  let editor = null;
  useEffect(()=>{
    editor = CodeJar(refToEditor.current, (e: HTMLElement) =>{}, {});
  },[refToEditor.current])
  return refToEditor;
}